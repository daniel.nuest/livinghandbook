---
name: Author agreement
description: Agreement authors have to sign/check to submit articles in the NFDI4Earth Living Handbook 

author: 
  - name: 
    orcidid: http://orcid.org/0000-0002-8186-3566

language: en
collection: Living Handbook

type: article

subject: 
  - gcmd:d62db12c-fdcf-40ec-a714-4fb7c615aebe

keywords:
  - Living Handbook 
  - NFDI4Earth

target_group: 
  - general

version: 0.1

---

!!! danger "Under review" 
    This article is currently under review. A revised version will become available soon.

# Author agreement

I hereby agree that the submitted work in all its original parts will be published under the [CC-BY 4.0 license](https://creativecommons.org/licenses/by-sa/4.0/). I warrant that the work is original and I am the author of the work. To the extent the work and the presentation incorporate text passages, figures, data or other material from other authors, I confirm that all re-used material is published under permissive licences that grant the right for such re-use and that the original material is attributed in accordance with the rules outset in these licences (check for licence compatability can be done, e.g., with [this tool](https://joinup.ec.europa.eu/collection/eupl/solution/joinup-licensing-assistant/jla-compatibility-checker). If unsure, please contact the LHB team via <nfdi4earth-livinghandbook@tu-dresden.de>). 

I hereby declare that the present work was produced by the indicated author(s) and no other sources and resources were used, except for the ones that I expressly indicated. All text passages and ideas quoted or borrowed from other sources of any kind have been clearly identified as such. 

If the article or any part therein was produced through an AI-based or AI-assisted technology, such as ChatGPT, I included a statement at the end of the article that identifies what was produced with which technology/product, and stating that all content made by it was checked and verified by the submitting author(s). 
