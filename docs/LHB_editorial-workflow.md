﻿---
name: Editorial Workflow
description: Specification of the editorial workflow in the Living Handbook.

author: 
  - name: Thomas Rose
    orcidid: http://orcid.org/0000-0002-8186-3566

language: ENG

collection: Living Handbook

type: article

subject: 
  - gcmd:d62db12c-fdcf-40ec-a714-4fb7c615aebe

keywords:
  - Living Handbook 
  - NFDI4Earth

target_group: 
  - general

version: 0.1

---

# Editorial Workflow

This document provides the detailed editorial workflow for the NFDI4Earth Living Handbook.

# Submission

Articles can be submitted to the Living Handbook only by mail. They can be either send to <nfdi4earth-livinghandbook@tu-dresden.de>, from where a member of the Editorial Board will forward it to the Living Handbook Editors. Or the author sends it directly to the Living Handbook Editors via <incoming+nfdi4earth-softwaretoolsarchitecture-livinghandbook-79252-issue-@mail.git.rwth-aachen.de>. In the future it is aimed that only the latter will be possible, to concentrate all communications at one place. In any case, the editorial workflow will be designed in a way that neither authors nor reviewers have to directly interact with Gitlab to remain as inclusive as possible. 

# From submission to publication

## Initial checks

After the article was received, an editor will be assigned to handle the article and the label “new article” added to the issue. The editor will perform the following, initial checks:

-   fits the scope of the Living Handbook
-   has a clear connection to ESS, NFDI4Earth or RDM
-   adheres to the style sheet and [author guidelines](LHB_author-guidelines.md)
-   is complete (e.g., all image files are included, links are working)
-   the author agreement is signed and license information for any figures etc. are included.
-   comes with correct metadata. Plese consult the NFDI4Earth Living Handbook metadata scheme to provide them. If necessary, reach out to the Editorial Board for further asisstance (this will hopefully be automated in future versions)
-   is appropriate in its style and quality, and sufficiently backed up by references

After the article passed these checks, it moves to [Final checks and publication](#final-checks-and-publication)) and the editor decides if the article requires a review (e.g., if a software developer presents its own software, this might not be necessary). A criteria list created by the editorial board will provide guidance for this decision. The editor sends the author his or her justified decision, and changes the article label to “final check” or “review”, respectively.

In case the checks are not satisfied, the editor provides appropriate feedback to the author, and asks for a resubmission. An article cannot be accepted for further processing before all checks are satisfied.

## Review

The editor assigns one reviewer for the article. The NFDI4Earth Living Handbook Editors will keep a record of persons willing to serve as reviewer on an ongoing basis to faciliate the search. The review process is open with the editors having an opt-out option (the article is already online at this stage, undiscloing the identity of the author). A review should be completed within three weeks. An editor can re-assign an article to a new reviewer, if the review process is not completed within six weeks. Once a reviewer is assigned, they will receive a copy of the article and submit their review through a mail address specific for the article-related issue. This will immediately notify the author, who then has the chance to revise the article accordingly within two weeks. 

After the author submitted the revised version, the editor assigned to the article will check if the points raised by the reviewer were sufficiently taken into account. Unless explicitly requested, the article will not be send again to the reviewer. 

After the article passed this check, it moves to [Final checks and publication](#final-checks-and-publication)). In case the check was not successful, the editor provides appropriate feedback to the author, and asks for a resubmission of the revised article until the editor is satisfied. 

<!-- The following text blocks may be used for the communication with the reviewers:
-->


## Final checks and publication

Once the article is ready to be included in the Living Handbook, the editor runs the following final checks:

-   Inclusion of the “under review”-badge, if the article is under review. Removal of the “under review”-badge after successful review.
-   Is everything correctly rendered?
-   Do all links have a valid target?
-   Is information from the Knowledge Hub successfully retrieved (if any)?

When all checks are satisfied, the editor creates the merge request to include the article into the Living Handbook. The managing editor or, if not available, a person appointed by them, checks and accepts the merge request and closes the issue. The author and the reviewer are notified that the article is now online in its revised version. 

# Article updates

This is currently identical to a new article, only that a copy of the existing article is downloaded from the git and re-submitted with the respective changes. The editorial board will be requested from the system to check and if necessary update articles after a defined period of time, by default one year. 

# Suggesting a new collection

A new collection can be suggested by anyone via email through the same channels as a new article. The suggestion needs to include the abstract/description of the collection as well as a suggested list of articles to demonstrate and justify this new collection.

The issue created through the mail by the Living Handbook Editor will be labelled “collection” and an editor assigned to handle the request. The editorial board discusses the suggestion, and may invite the suggesting person to this discussion. Following a positive vote, the collection article is created by the handling editor, and checked and published by the managing editor just like any other article. The suggesting person together with the handling editor will update the metadata of the respective articles to include them in the new collection. 
