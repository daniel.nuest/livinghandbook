﻿---
name: The DataPro GeoPlatform
description: Presentation of the DataPro GeoPlatform

author: 
  - name: Dominik C. Hezel
    orcidid: 0000-0002-5059-2281

language: ENG

type: software

subject: 
  - dfgfo:316-01
  - gcmd:ff2d6f74-959c-42f2-9905-dd942fd80598
  - gcmd:906e647b-2683-4ae7-9986-1aea15582b52
  - SDN:P13::GTER0045
  - gcmd:1d550f3a-1c8c-4ef5-beff-74cfe7794f12

keywords:
  - microprobe
  - tools

target_group: 
  - student
  - researcher

version: 0.1

---

# The DataPro GeoPlatform

DataPro is a webpage that collects and structures various types of geochemical, cosmochemical, and data science software used for data acquisition & processing, research, and teaching at the Institut für Geowissenschaften at the Goethe University Frankfurt. The webpage further includes dedicated tools for e.g., archiving standards, preparing measurement sessions, or accessing distributed databases. These software are highly diverse in complexity, length and functionality. The aim of DataPro is to provide simple orientation to these software products via a conventional webpage that includes detailed descriptions, documentations, and tutorials of the available software. DataPro is available through the free and open [geoplatform.de website](https://hezel2000.quarto.pub/ifg-data-processsing-center/). The entire web-application is facilitated using Python, Quarto, Streamlit and a public GitHub repository, which is all documented on the [geoplatform.de website](https://hezel2000.quarto.pub/ifg-data-processsing-center/).

| Tool       | DataPro GeoPlatform |
|------------|---------------------|
| URL        | [geoplatform.de website](https://hezel2000.quarto.pub/ifg-data-processsing-center/) |
| Founded    | 2023                |
| Located    | Goethe University Frankfurt | 
| Frameworks | Python, Quarto, Streamlit, GitHub |

## References
Hezel DC and Höfer HE 2023. A web-based, open tool for the preparation and data reduction of electron microprobe measurements. Goldschmidt abstract
