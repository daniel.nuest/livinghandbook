# Welcome to the NFDI4Earth Living Handbook

The NFDI4Earth Living Handbook provides (i) an encyclopaedia-style documentation for all aspects related to the NFDI4Earth, its services and outcomes in a human readable form, and (ii) aims to structure and harmonise all information related to data management and data science approaches in the Earth System Sciences (ESS) in a community-driven effort. 

The Living Handbook is compiled in a community-driven process, meaning that the individual members of the ESS community provide the knowledge through articles and modify the articles to add further information or to keep them up to date. The Living Handbook Editorial Board supports the community in this effort, improves the editorial workflow and can set impulses in the development of the content. At the same time, the Living Handbook is the archive of the User Support Network. 

The Living Handbook is still in a very early stage. If you are curious how it currently works or are even willing to help optimising it by serving as one of its first authors, please have a look on [the collection of the respective articles](LHB.md). 
