---
name: The NFDI4Earth Living Handbook
description: A collection of articles related to the NFDI4Earth Living Handbook.

author: 
  - name: Thomas Rose
    orcidid: http://orcid.org/0000-0002-8186-3566

language: ENG

type: Collection

collection: 
  - NFDI4Earth

subject: 
  - gcmd:d62db12c-fdcf-40ec-a714-4fb7c615aebe

keywords:
  - NFDI4Earth
  - Living Handbook

target_group: 
  - researcher

version: 0.1

---

# The NFDI4Earth Living Handbook

The NFDI4Earth Living Handbook provides (i) an encyclopaedia-style documentation for all aspects related to the NFDI4Earth, its services and outcomes in a human readable form, and (ii) aims to structure and harmonise all information related to data management and data science approaches in the Earth System Sciences in a community-driven effort. 

With the articles listed below, you can learn how the Living Handbook works and how you can contribute to it. 

* [Author guidelines](LHB_author-guidelines.md)
* [Author agreement](LHB_author-agreement.md)
* [Editorial Workflow](LHB_editorial-workflow.md)
* [Concept of the Editorial Board](LHB_editorial-board-concept.md)
* [Collection articles](LHB_collections.md)
