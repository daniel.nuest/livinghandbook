﻿---
id: art_Hedden2010

type: recommended_article

authors:
  - Michael Finkel
  - http://orcid.org/0000-0002-8186-3566

language: en
collection: Metadata_Management

keywords:
  - research data management
  - metadata
  - taxonomy
  
authors_orig:
  - literal: "Hedden, Heather"  

title: "Taxonomies and controlled vocabularies best practices for metadata"

issued: 2010-Oct-10

pub_src: Journal of Digital Asset Management 	 		

pub_src_abbrev:  J. Digit. Asset. Manag.

pub_url: https://doi.org/10.1057/dam.2010.29

doi: 10.1057/dam.2010.29
---

*Recommended Original Article*

Taxonomies and controlled vocabularies best practices for metadata
-------------------
from <author>Heather Hedden</author>

__Abstract:__ <abstract>Taxonomies or controlled vocabularies are used in descriptive metadata fields to support consistent, accurate, and quick indexing and retrieval of digital asset content. Designing metadata and controlled vocabularies is an integrated process that takes into consideration which and how many metadata fields will make use of controlled vocabularies. Synonyms (non-preferred terms) and hierarchies are methods to help users find the right term within a large controlled vocabulary. Best practices for creating non-preferred terms and hierarchies are explained.</abstract>

*Published in:* <journal>Journal of Digital Asset Management</journal> (<year>2010</year>), Vol. <volume>6</volume>[<issue>5</issue>], <pages>279–284</pages> 

__Link:__ [<url>https://doi.org/10.1057/dam.2010.29</url>](https://doi.org/10.1057/dam.2010.29)

## Comment:
<notes>This article advocates the use of taxonomies or controlled vocabularies in metadata annotation. And that we have to think metadata schemes and taxonomies/vocabularies together: vocabulary design needs to be integrated with the metadata strategy. In her conclusion, Heather that this task requires a multidisciplinary team of experts to implement.</notes> 

*Michael Finkel*