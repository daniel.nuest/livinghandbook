---
name: LIAG GeotIS
description: The geothermal information system GeotIS

author: 
  - name: Ira Gerloff

language: ENG

collection: Repositories

type: repository

subject: 
  - dfgfo:315-01
  - gcmd:ef9a4b5c-4803-485d-b3ca-9693c921f65d
  - gcmd:258d6984-ff0c-40d5-9dc5-673d211e21e7
  - gcmd:ec2bf43d-2525-439e-bbbe-0db758e71965
  - SDN:P21::MS11577
  - SDN:P03::GTHM
  - SDN:P21::MS4309

keywords:
  - GeotIS 
  - LIAG
  - Geothermal atlas
  - Geophysics
  - geologic 3D model
  - subsurface temperature data
  - geotherm statistic

target_group: 
  - researcher

version: 0.1

---

<!-- this is an article requires an info-box populated from the Knowledge Hub with data harvested from Re3data -->

# The Geothermal Information System GeotIS

__Summary:__ <summary>The geothermal information system GeotIS can see as a digital geothermal atlas addressing the needs of researchers, project planners and investors. It is operate by the Leibniz Institute for Applied Geophysics (LIAG) and covers Germany and Upper Austria. The available data has been compiled and validated by the LIAG and many of its research partners since it went online in 2007. 
The LIAG accepts new or updated maps, geologic 3D models and other data if they meet the high quality standards of GeotIS. Project reports, references and data annotations are linked to the data objects and are visible to all users.
The web interface of GeotIS can be access with any standard browser and is free of charge. Many subsurface parameters relevant for geothermal energy exploration can be visualised in regional contexts. GeotIS provides interactive tools to generate subsurface cross sections, maps and tables. These tools support scientists in obtaining relevant data for research projects and help project planers to identify promising locations for geothermal projects. The most important data relate to hydraulic conductivity, temperature, faults and depth of rock formations. Especially the predictive 3D subsurface temperature model reaching from ground level to 5 km below sea level is use very frequently.
GeotIS also provides up-to-date figures on geothermal facilities (e.g. installed geothermal capacity, annual energy production, maximum flow rates, reservoir temperature etc.) and annual geothermal energy statistics for Germany, which are used for national energy statistics on renewable energy, as well as in international reports.</summary>


__Homepage:__ <https://www.geotis.de>


<iframe src="../pdf/DRUCK_190412_Geotis_ENGL_Liag_002.pdf" width="90%" height="800px" ></iframe>
