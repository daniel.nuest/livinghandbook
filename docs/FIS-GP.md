﻿---
name: Fachinformationssystem Geophysik (FIS-GP)
description: Geophysics Information System (FIS-GP)

author: 
  - name: Ira Gerloff

language: ENG

collection: Repositories
     
type: repository

subject: 
  - gcmd:8f9d66e9-f65d-41c6-9640-90bd3e155bf8
  - dfgfo:315-01
  - gcmd:ef9a4b5c-4803-485d-b3ca-9693c921f65d
  - gcmd:f1f84fc8-d242-4f97-bb7d-77b68631273e

keywords:
  - FIS-GP 
  - Leibniz-Institut für Angewandte Geophysik
  - LIAG
  - Geophysics
  - metadata
  - measurement
  - data interpretation
  - subsurface
  - Geology
  - Palaeontology
  - Geodesy
  - Geography
  - borehole geophysics
  - drilling
  - geoelectricity
  - geothermal energy
  - gravimetry
  - helicopter geophysics
  - magnetism
  - measurement
  - petrophysics
  - seismics
  - temperature

target_group: researcher

version: 0.1

---

<!-- this is an article requires an info-box populated from the Knowledge Hub with data harvested from Re3data -->
<!-- FIS-GP application is currently out of service. Service will be restored in the near future -->

# Geophysics Information System (FIS-GP)

__Summary:__ The Geophysics Information System (FIS GP) is used to store and provide geophysical metadata, measurements and data interpretations covering Germany and adjacent areas. The repository is operated by the Leibniz Institute for Applied Geophysics (LIAG) and can be used free of charge. The data provided originates from many sources including the LIAG itself, Geological Surveys, research institutes, universities and industry. The aim is to establish and maintain a comprehensive data stock on the subsurface throughout Germany. The starting point for the development of the FIS-GP was the decision of a working group on information systems of the Geological Surveys in Germany in 1989. However, it took several years until first data compilations were initiated. In 2002 FIS-GP went online. 
FIS-GP includes an import program, a web interface and a database, which is subdivided in a general part (superstructure) with metadata of all methods and in several subsystems (borehole geophysics, 1D/2D geoelectrics, gravimetry, magnetics, temperatures, 2D seismics, VSP, aero geophysics, petrophysics, transient electromagnetics and SkyTEM). Particularly noteworthy is the subsystem temperatures, which contains a unique collection of measured and corrected subsurface temperatures in Germany. Everyone can login as guest user and may retrieve metadata, free measurements and data interpretations. Some of the records are confidential and can only be accessed as registered user with permission of the data providers.
The web interface of FIS GP consists of the components FIS-GP-Search and FIS-GP-Viewer. FIS-GP-Search offers a menu- and form-based user interface with search, print, visualization, and download options. With FIS-GP-Viewer geophysical data objects can be displayed and selected on interactive maps.



__Link:__ <https://www.fis-geophysik.de/>


<iframe src="../pdf/Agemar_FIS-GP_16-9.pdf" width="90%" height="800px" ></iframe>
