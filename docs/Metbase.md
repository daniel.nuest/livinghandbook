---
name: The MetBase Meteorite Database
description: The MetBase Meteorite Database

author: 
  - name: Dominik C. Hezel
    orcidid: 0000-0002-5059-2281

language: ENG

collection: 
  - Repositories
  - "NFDI4Earth Pilots"

type: repository 

subject: 
  - dfgfo:316-01
  - dfgfo:311-01
  - gcmd:96be1efc-d5e2-423d-8ade-00b3d454244d

keywords:
  - Database
  - Meteorites
  - Astromaterials
  - "NFDI4Earth Pilot"

target_group: 
  - researcher

version: 0.1

---

<!-- this is an article requires an info-box populated from the Knowledge Hub with data harvested from Re3data --># The MetBase Meteorite Database 

![Metbase logo](img/MetBase_Logo.png)

MetBase was established in the 1990s and hosted by a private collector in Bremen. Among others, the database consisted of more than 500.000 individual data of, for instance, meteorite bulk and component chemical, isotopic and physical properties. The database further held more than 90,000 references from 1492 until today. In 2006, the high value of the database was acknowledged by the Meteoritical Society with its Service Award. The technical foundation of MetBase has been migrated to an SQL-based format in around 2016. At the same time, the interface moved to a web-based framework with innovative access and visualisation tools. The portfolio was expanded by an online course and a daily aggregation of the newest cosmochemical research papers ([cosmochemistry-papers.com](https://cosmochemistry-papers.com/)).
The MetBase metadata and its overall implementation, however, required an update to meet the ever increasingly important FAIR (Findable, Accessible, Interoperable, Reproducible) principles. In the early 2020s, NASA started the AstroMat database initiative for their Apollo and space-craft sample return missions. This was an opportunity and literal match made in have to join forces and align the MetBase and AstroMat schemata and using a modern set of metadata that is also used by large geochemical databases such as EarthChem or GEOROC, thereby defining a quasi-standard. 

The web-interface of MetBase is still online, and in fact its technical foundation was rewritten, and its capabilities improved and expanded in 2023. It is since then possible to visualise data from multiple databases such as AstroMat, GEOROC, or AusGeochem simultaneously. At the same time it became part of the then newly formed [geoplatform.de](https://hezel2000.quarto.pub/ifg-data-processsing-center/). 

The alignment of the MetBase schemata with AstroMat, as well as the new web-interface have been funded with an NFDI4Earth pilot grant.

Read more about Metbase: [@2022LPICo2695.6409H; @2022EGUGA..2413457H; @2021LPICo2654.2013H]
