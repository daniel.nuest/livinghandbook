---
id: art_Gudmundsson2018
type: article
authors:
  - Michael Finkel
  - http://orcid.org/0000-0002-5268-5203
authors_orig:
  - literal: "Gudmundsson, Lukas and Do, Hong Xuan and Leonard, Michael and Westra, Seth"
title: "The Global Streamflow Indices and Metadata Archive (GSIM) - Part 2: Quality control, time-series indices and homogeneity assessment"
issued: 2018-04
pub_src: Earth System Science Data
pub_src_abbrev: Earth Syst. Sci. Data
pub_url: https://doi.org/10.5194/essd-10-787-2018
doi: 10.5194/essd-10-787-2018
---
*Recommended Original Article*

The Global Streamflow Indices and Metadata Archive (GSIM) - Part 2: Quality control, time-series indices and homogeneity assessment
-------------------
from <author>Lukas Gudmundsson, Hong Xuan Do, Michael Leonard, and Seth Westra</author>

__Abstract:__ <abstract>This is Part 2 of a two-paper series presenting the Global Streamflow Indices and Metadata Archive (GSIM), which is a collection of daily streamflow observations at more than 30 000 stations around the world. While Part 1 (Do et al., 2018a) describes the data collection process as well as the generation of auxiliary catchment data (e.g. catchment boundary, land cover, mean climate), Part 2 introduces a set of quality controlled time-series indices representing (i) the water balance, (ii) the seasonal cycle, (iii) low flows and (iv) floods. To this end we first consider the quality of individual daily records using a combination of quality flags from data providers and automated screening methods. Subsequently, streamflow time-series indices are computed for yearly, seasonal and monthly resolution. The paper provides a generalized assessment of the homogeneity of all generated streamflow time-series indices, which can be used to select time series that are suitable for a specific task. The newly generated global set of streamflow time-series indices is made freely available with an digital object identifier at [PANGAEA](https://doi.pangaea.de/10.1594/PANGAEA.887470) and is expected to foster global freshwater research, by acting as a ground truth for model validation or as a basis for assessing the role of human impacts on the terrestrial water cycle. It is hoped that a renewed interest in streamflow data at the global scale will foster efforts in the systematic assessment of data quality and provide momentum to overcome administrative barriers that lead to inconsistencies in global collections of relevant hydrological observations.</abstract>

*Published in:* <journal>Earth System Science Data</journal> (<year>2018</year>), Vol. <volume>10</volume>[<number>2</number>], <pages>787-804</pages> 

__Link:__ [<url>https://doi.org/10.5194/essd-10-787-2018</url>](https://doi.org/10.5194/essd-10-787-2018)

## Comment:
<notes>This is part 2 of a very interesting work showing how quality control aspects can be integrated in large databases that are fed from various sources.</notes> 

*Michael Finkel*
