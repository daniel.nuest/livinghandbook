---
name: Author guidelines
description: Author guidelines for the NFDI4Earth Living Handbook.

author: 
  - name: Thomas Rose
    orcidid: http://orcid.org/0000-0002-8186-3566

language: ENG

collection: Living Handbook

type: article

subject: 
  - gcmd:d62db12c-fdcf-40ec-a714-4fb7c615aebe

keywords:
  - Living Handbook 
  - NFDI4Earth

target_group: 
  - general

version: 0.1

---

# Author guidelines for the NFDI4Earth Living Handbook

These are the author guidelines for the NFDI4Earth Living Handbook. They are continuously updated as the Living Handbook evolves.

## Submission

Articles can be submitted to the Living Handbook only by mail. They can be either send to <nfdi4earth-livinghandbook@tu-dresden.de>, from where a member of the Editorial Board will forward it to the Living Handbook Editors. This address should also be used for general inquiries. Alternatively, articles can be sent directly to the Living Handbook Editors via <incoming+nfdi4earth-softwaretoolsarchitecture-livinghandbook-79252-issue-@mail.git.rwth-aachen.de>. 

## Content
Articles in the Living Handbook summarise knowledge and are typically not original research. However, it's content must be verifiable by providing reliable sources. These are ideally cited in line and listed at the end of the article. 

When writing an article, please use the "spiral out" approach as much as possible. "Spiral out" means:

-   start with the core of a topic
-   develop *all* its main aspects briefly
-   cover some or all of these main aspects in detail

This concept allows readers unfamiliar with the content to acquire general knowledge first, before engaging with the expert knowledge in the article. Moreover, it makes it easy for the reader to stop reading after they gathered sufficient information. Guidance on good scientific writing can be found at e.g. <https://doi.org/10.1371/journal.pcbi.1004205> . Please stick to these rules as close as sensible. 

There is no minimum or maximum length for articles. We typically aim for an article lengths between 500 and 5000 words. It increases the readability of a complex topic if it is split into multiple articles rather than cramming all knowledge into one article. If in doubt, it is recommended to create multiple articles.

Any element other than article text, info-boxes, or content directly embedded into the text (e.g. code snippets or formulas) must have a caption. Elements embedded in the text must be discussed in the text. 

Linking between articles is explicitly encouraged. Links between articles are written as `[Link text](article.md)`. To link to specific sections within an article, write `[Link text](article.md#section)`. The same syntax is used to link to external pages and sections within them by replacing `article.md` with the full URL of the page.  

## Collections
Collections bundle topically related articles. A collection has the formal structure of a regular article that starts with a brief introduction text to the topic of the collection, followed by a maybe structured link-list to the included articles. You may create synergies by contextualising your article by suggesting an appropriate topical collection in which your article could be included alongside other articles. You can learn more about collection in the [collection article](LHB_collections.md)

## Interactive content
This section is work in progress... 

## Licences
The article and all its content, including any additional files such as images, must be made available under a [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/) licence.  

Linked files, such as videos or external figures, must be referenced in the caption according to the license they were published. In any case, the reference must contain at least the author, a link to the webpage the work is taken from, and the name of the licence under which the work was published. The name of the licence shall be a link to the text of the respective licence. 

The author declares that the material used in the article is suitably licensed or can be published under them. The author further declares that appropriate credit is given to all sources used for the article and that any parts copied from other sources is marked as quote. Confirmations will be recorded by the author checking or signinf an author agreement.

## Accessibility
The following points should be considered to make the content of articles as accessible as possible: 

- Provision of alternative text to any non-text element such as figures or charts
- Use of colour-blind friendly colour schemes. See e.g. <https://davidmathlogic.com/colorblind> for basic guidance on colouring for colorblindness, and <http://www.vischeck.com/> to check how your figure might look like in different types of colour blindness. The tool <https://colorbrewer2.org> is a great resource for all different color schemes, including colour-blind friendly ones. 
- Ensure high contrast of graphical elements
- Clear and coherent structure of the article 

<!-- TR: the numbering of images remains to be discussed. It is error prone if not handled automatically (especially when articles get updated) and e.g. Wikipedia manages surprisingly well without figure numbers) -->

There are numerous pages available to learn more about how to make a webbased article accessible. A good starting point might be the [page of the W3C about this topic](https://www.w3.org/WAI/fundamentals/accessibility-intro/). 

## Technical specifications
Articles should be submitted in markdown (`.md`) if possible. Otherwise, plain text (`.txt`) or formatted text in the formats `.rtf`. `.doc`, and `.docx`) is also accepted. in formatted texts, formatting should be restricted to a minimum. Any images and other media must be provided as separate files. Please reach out to the editorial board via <nfdi4earth-livinghandbook@tu-dresden.de> if you need help with e.g. the inclusion of interactive content. 

Files other than the text should be submitted in the following formats: 

| Type | File type |
|-------|-------|
| Raster graphics | `.png` |
| Vector graphics | `.svg` |
| Diagrams, Flowcharts, ... | `.svg` or as [mermaid](https://mermaid.js.org/) code in the article. |
