---
name: NFDI4Earth One pager
description: The NFDI4Earth One pager are short concepts of the different products. 

author: 
  - name: Thomas Rose
    orcidid: http://orcid.org/0000-0002-8186-3566

language: ENG

collection: N4E_Publications

type: report

subject: 
  - gcmd:d62db12c-fdcf-40ec-a714-4fb7c615aebe

keywords:
  - Living Handbook 
  - NFDI4Earth
  - Knowledge Hub
  - Software Architecture
  - USer Support Network
  - OneStop4All
  - EduTrain

target_group: 
  - general

identifier: https://doi.org/10.5281/zenodo.7583595

version: 0.1

---

# NFDI4Earth One pagers

The mission of NFDI4Earth is to address the digital needs of the Earth System Sciences (ESS). We develop several components, services and concepts within NFDI4Earth. To improve the internal and external communication, we provide One-Pagers for selected central components, services and concepts, which describe them from the usage / user perspective on one to a maximum of two pages. 

The One-Pagers follow a common structure, starting with the overall aim of the related NFDI4Earth software component or concept. After that, we briefly describe core problem(s) and innovative approaches to solutions. NFDI4Earth is a community-driven project. We therefore identify users of the components / concept and the intended benefits for them. 

Moreover, innovations have adoption units (see [Rogers, 2003](LHB_one pager.md#references)). These can be individuals (e.g., a researcher) or organizations (e.g., a research institute that commits to operate a service after its development). We point out the intended adoption units with respect to the below-mentioned expected outcomes and evaluation criteria.

# References
[Rogers, E. M. (2003), Diffusion of innovations. Simon and Schuster. ISBN: 9780743222099](https://www.simonandschuster.com/books/Diffusion-of-Innovations-5th-Edition/Everett-M-Rogers/9780743222099) 

<!-- Hopefully References will later be handled through a bibtex file or so, ensuring coherent formatting and easy insertion -->

<iframe src="../pdf/N4E_One pager.pdf" width="90%" height="800px" ></iframe>