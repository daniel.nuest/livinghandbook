---
name: Collections
description: Collections are used to structure the content of the Living Handbook. 

author: 
  - name: Thomas Rose
    orcidid: http://orcid.org/0000-0002-8186-3566

language: ENG

collection: Living Handbook

type: article

keywords:
  - Living Handbook 
  - NFDI4Earth

target_group: 
  - general

version: 0.1

---

# Collections

Collections are articles of the Living Handbook that structure its content. They consist of a short abstract or summary about the topic, followed by a list of the articles that belong to this collection. The list is automatically created from the metadata of the articles.  

Collections aim to help readers in gaining a better overview on a specific topic by providing a list of relevant articles. Consequently, not all articles are part of collections. Collections can belong to collections, allowing to introduce some kind of hierarchy in the content of the Living Handbook. 

![Structure of Living Handbook Collections](img/collections.png)

*Schematic visualisation of how collections structure the content of the Living Handbook.* 