---
name: Template article
# The title of the article.

description: 
# A brief summary of the article, max. 300 characters incl. spaces.

author: 
  - name: Jane Doe
    orcidid: https://orcid.org/0000-0002-1584-4316
  - name: John Doe
    orcidid: https://orcid.org/0000-0001-2345-6789
# The author(s) of the article. The name must be given, the ORCID should be given. As many authors as necessary can be listed. 

language: ENG
# The language of the article. Accepted values are:  
# ENG for English
# DEU for German

collection: 
  - Collection1
  - Collection2
# The identifiers of the NFDI4Earth Living Handbook collection(s), the article belongs to. 
# Internal note: Where can they be found?

type: Template
# The type of the article. Acceptable terms are: 
# article: The most generic category. 
# best practice: A defined procedure how a defined problem can be solved which is a (de-facto) standard or widely accepted in the community
# collection: A collection article
# workflow: A step-by-step description of a procedure
# recommended_article: a short review of a publication 
# Please refer to the author guidelines (###) for a description of the different types.  

subject: 
  - dfgfo:313-02
  - gcmd:7aafe1ad-b785-4805-ac76-2aa102cdb7e4
# A list of controlled terms describing the subject of the article, given as their identifiers. Acceptable vocabularies are any terms from the: 
# DFG subject ontology: https://terminology.tib.eu/ts/ontologies/dfgfo/terms
# NASA GDMC Keywords: https://gcmd.earthdata.nasa.gov/KeywordViewer
# NERC Vocabulary Server: https://vocab.nerc.ac.uk/search_nvs/sxv/?searchstr=&options=identifier,preflabel,altlabel,definition
keywords:
  - keyword1
  - keyword2
# A list of terms to describe the article's topic more precisely. 

target_group: 
  - student
  - researcher
# The group(s) of readers, the article is aiming for or might be of particular interest. Acceptable terms are: 
# researcher
# data_curator
# data_steward
# policymaker
# student
# general
# Please refer to the author guidelines (###) for a description of the different types. 

identifier: 
# If the article has already a unique identifier such as an DOI, provide it here as URL (e.g., https://doi.org/10.1001/12345) . 

version: 0.1
# The version of the article; will be updated by the editor. 

---

# Template article

This article provides you with the most important information on how to write an article for the NFDI4Earth Living Handbook. this includes the most common features of Markdown, the lightweight markup language its articles are written in. 

## Metadata
The top part of this document, enclosed in `---`, contains the metadata associated with the articles, i.e., information that describes your article. Please fill it in/replace the examples according to the explanation provided for each metadatum. Afterwards, you can delete any line starting with `#`. 

## Structure of NFDI4Earth Living Handbook articles
...

## Media
...

## Writing basic Markdown
...
