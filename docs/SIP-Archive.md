﻿---
name: SIP Archive
description: Archiv für Spektral Induzierte Polarisation

author: 
  - name: Ira Gerloff

language: en
collection: Repositories

type: repository

subject: 
  - gcmd:ef9a4b5c-4803-485d-b3ca-9693c921f65d
  - dfgfo:3
  - SDN:P02::RSIS

keywords:
  - SIP 
  - Leibniz-Institut für Angewandte Geophysik
  - LIAG
  - Spectral Induced Polarization
  - complex electrical rock properties
  - measurement
  - geophysics
  - petrophysics
  - Geology
  - geoelectricity
  - raw data

target_group: 
  - researcher

version: 0.1

---

<!-- this is an article requires an info-box populated from the Knowledge Hub with data harvested from Re3data -->


# Archiv für Spektral Induzierte Polarisation

__Summary:__ The SIP archive is a research data repository for measurement data obtained using the method of spectral induced polarisation (SIP).  
The development started in spring 2015, when at that time this method was reintroduced into the repertoire of petrophysics methods at the Leibniz Institute for Applied Geophysics (LIAG). The web application was also made available to the scientific community in Germany and, as the only repository for this method, generates significant added research value for all users. The SIP archive went online at the beginning of 2017. Using this infrastructure, searches for measurement data and sample data are made possible with the help of metadata. 
The SIP-Archive is operated by the LIAG and provides a safe, long term archive and database for SIP laboratory data. It is web based, easy and free to use and self-administrated, in order to ensure maximum control over individual and institutional data sets. The institutions manage their data completely on their own and decide on their own responsibility whether to release their measurement data for other institutions to download. 
In addition, this database provides functionalities to exchange and provide measurement data and metadata with respect to national (German) and European guidelines for good scientific practice.
The institutions involved so far belong to the working group "Induced Polarisation" (AK IP) of the German Geophysical Society (DGG). Use is only permitted via registration by the participating institutions.



__Link:__ <https://www.sip-archiv.de/>


