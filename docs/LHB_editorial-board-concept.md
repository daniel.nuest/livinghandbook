---
name: Editorial Board Concept
description: Concept with task and details of the work of the NFDI4Earth Living Handbook Editorial Board

author: 
  - name: Thomas Rose
    orcidid: http://orcid.org/0000-0002-8186-3566

target_group: 
  - editorial board
  - general

language: ENG

collection: Living Handbook

type: article

subject: 
  - gcmd:d62db12c-fdcf-40ec-a714-4fb7c615aebe

keywords:
  - Living Handbook 
  - NFDI4Earth

target_group: 
  - general

version: 0.1

---
!!! danger "Under review" 
    This article is currently under review. A revised version will become available soon.

# Concept of the NFDI4Earth Living Handbook Editorial Board

## Aim and tasks
The NFDI4Earth Living Handbook provides (i) an encyclopaedia-style documentation for all aspects related to the NFDI4Earth, its services and outcomes in a human readable form, and (ii) aims to structure and harmonise all information related to Data Science approaches in the Earth System Sciences in a community-driven effort. 
The Editorial Board curates the content of the NFDI4Earth Living Handbook. Content curation entails 

* Prioritization of and closing gaps in content of the NFDI4Earth Living Handbook, especially in its initial phase 
* Ensuring that articles submitted to the Living Handbook are appropriate in their scope, quality and style
* Supporting authors in bringing their articles online
* Organising the peer-review process of articles if necessary
* Optimisation of the editorial workflow
<!-- * Organising events to promote the NFDI4Earth Living Handbook, to build a community for it, and to incentivize the community to submit articles -->

## Members
Anyone fulfilling the following requirements can become member of the NFDI4Earth Living Handbook Editorial Board: 

* Part of the Earth System Science community and ideally actively working in contexts related to research data management
* Capability to work independently on tasks in a timely manner, including being informed for discussion during the meeting and participation in discussions between the meetings
* Familiarity with working in GitLab or willingness to learn the respective workflows (training sessions will be offered) 

Members will be listed on the web page of the Editorial Board. In addition, their contribution to the NFDI4Earth Living Handbook is acknowledged wherever possible, e.g., in reports about the NFDI4Earth Living Handbook and on each article that was handled as editor.

## Internal organisation
Members of the Editorial Board are welcome to pick the tasks they feel most comfortable with as long as all tasks are covered. 
Editorial Board members will elect one of them as managing editor. This role passes on to another editorial board member every six months to encourage a wide variety of members in bringing forward their ideas and to set new accents in the Living Handbook. Further, the managing editor will coordinate the work of the editorial board and cast the final vote if it is impossible to reach a consensus and votes resulted in a draw. 
The editorial board in general and the managing editor in particular are supported by the NFDI4Earth measure M3.3 in the coordinative tasks and organisation of the events and other actions planned by the editorial board. 

## Work mode
The Editorial Board meets every four weeks. Meetings are strictly restricted to one hour. If necessary, additional meetings for, e.g., specific tasks will be organised with the persons actively involved in them. The agenda will be shared at least one week in advance by the M3.3 team. Every Editorial Board member can suggest topics until two days before the meeting. The minues are kept by the M3.3 team in the GitLab repository of the NFDI4Earth Living Handbook. 

Most work and discussions will be done between the meetings in a way that provides the most flexibility for each member’s schedule. For doing so, the issue feature of the Editorial Board’s GitLab repository is used, which can also be operated almost exclusively by e-mail. Meetings are reserved for general updates, formal approval of documents, and discussions too complex for the asynchronous format. Discussions will be restricted to one topics per meeting. 

