# LivingHandbook

The NFDI4Earth Living Handbook provides (i) an encyclopaedia-style documentation for all aspects related to the NFDI4Earth, its services and outcomes in a human readable form, and (ii) aims to structure and harmonise all information related to data management and data science approaches in the Earth System Sciences in a community-driven effort. 

To install and work on the Living Hanndbook on your local machine, follow the instructions below. 

## Prerequisites
* Basic knowledge about working with git, especially retrieving remote changes via `pull`, and adding your changes via `add`, `commit`, and `push`. You can learn about them e.g., at <https://uidaholib.github.io/get-git/3workflow.html>

### Connecting Gitlab to your computer
* You must have git installed to work on the Living Handbook (alternatively, interact with it here). 
  * To check if git is already installed an your system and to configure git, follow <https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html>
  * You can download git from <https://git-scm.com/>
* It is strongly recommended to set up a SSH connection between your RWTH Aachen GitLab Account and your computer. Learn about this is done at https://docs.gitlab.com/ee/user/ssh.html>
  * The instance URL of the RWTH Aachen Gitlab is `git.rwth-aachen.de`. 

## Clone the repository on your computer
* Open the Console/Git Bash in the folder you want to clone the repository in
* Enter `git clone git@git.rwth-aachen.de:nfdi4earth/livinghandbook/livinghandbook.git`
* A folder `livinghandbook` should now be present in your folder

### Install MkDocs
* If not installed yet, install Python
  * Enter `python --version` (for Windows: alternatively `py --version`) into the console
  * If the output is `Python 3.11.1` or similar, you have Python installed. If not, download and install Python following the instructions at <https://wiki.python.org/moin/BeginnersGuide/Download>. On Windows, make sure to check *Add Python to PATH* in the installer. 
* Install the latest version of Python's package manager pip with `python -m pip install --upgrade pip`. 
* Install MkDocs by running the following command in the console with the folder `livinghandbook` as working directory: Install MkDocs with `pip install -r requirements.txt`

## Work on the Living Handbook

### Basics
* The content of the Living Handbook lives in the `docs` folder of `livinghandbook`
* All articles are markdown files (file ending `.md`). You can learn the basics on how to write markdown at <https://www.markdownguide.org/basic-syntax/> 
* Don't use spaces in file names, use dashes instead. 

### Writing articles
* On your computer
  * Create a new file with ending `.md` in the `docs` folder or duplicate an existing file. 
  * To modify an existing article, open the respective `.md` file with the text editor or IDE of your choice. 
* In the browser 
  * Navigate to the `docs` folder, create a new file in it and make sure its name includes the `.md` ending. For how to create new files in GitLab see <https://about.gitlab.com/blog/2016/02/10/feature-highlight-create-files-and-directories-from-files-page/#create-a-file>
  * To modify an existing article, open the article and click one of the options in the blue button on top of the file. 

<!-- 
* Add/update the YAML header. It should look like 
```
--- 
title: "My title"
---

# The actual article starts here
```
-->

* Write the article 
* If you want to add images or other media, save them in the `img` folder of `docs` 
* Don't forget to push the finished/modified article to the GitLab repository! 

### Creating a live version of the Living Handbook
* Open the console in `livinghandbook`
* Enter `mkdocs serve`
* A new tab should open in your browser. If not, open the browser and enter the address `http://127.0.0.1:8000/`
* Use the search bar in the upper right corner of the Living Handbook webpage to find your article  (this is the most universal way, it will get more convenient with the Living Handbook evolving)
* Pressing `CTRL+C` in the terminal shuts down the virtual server

In case of any questions, please contact [Thomas Rose](t.rose@em.uni-frankfurt.de) or [Ralf Klammer](ralf.klammer@tu-dresden.de)
